const http = require('http');
const fs= require('fs');

const server = http.createServer((req,res) =>{
    
    const url = req.url;
    const method = req.method;

    if(url === '/'){
    res.setHeader('Content-Type','text/html');
    res.write('<html>');
    res.write('<head><title>Enter Message</title></head>')
    res.write('<body><form action="/message" method="POST"><label>Enter the Message</label><input name="message" type="text"><button type="submit">Submit</button></form></body>')
    res.write('</html');
     return res.end();
    }

    if(url === '/message' && method === 'POST'){
        const body=[];
        
        req.on('data',(chunk)=>{
            body.push(chunk);
        });

        req.on('end',()=>{
            const parsedBody = Buffer.concat(body).toString();
            const value = parsedBody.split('=')[1];
            fs.writeFile('message.txt',value, err => {
                res.statusCode(302);
                res.setHeader('Location','/');
                return res.end();
            });
        });
    }
    
    res.setHeader('Content-Type','text/html');
    res.write('<html>');
    res.write('<head><title>First Node.js Project</title></head>')
    res.write('<body><h1> Hello Node.js</h1></body>')
    res.write('</html');
    res.end();
});

server.listen(3000);