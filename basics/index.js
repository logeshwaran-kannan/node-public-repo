console.log('Hello World');

const add = (a,b) => a+b;

const squareTheNumber = a => a*2;

console.log(add(1,2));

console.log(squareTheNumber(5));


const s = {
    name:'logesh',
    age:26,
    myDetail(){
        console.log('Name is ' + this.name + ' ,age is ' + this.age)
    }
}

s.myDetail();

  
const {age} = s;

console.log(age);