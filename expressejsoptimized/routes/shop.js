const express = require('express');
const path = require('path');
const adminData = require('./admin');
const routes = express.Router();

//Shop routing
routes.get('/', (req,res,next) => {
    const prods = adminData.products;
    res.render('shop',{
        pageTitle:'Shop',
        prods : prods, 
        path:'/', 
        hasProducts : (prods.length > 0)
    });
});

module.exports = routes;
