const express = require('express');

const app = express();

// app.use((req,res,next) => {
//     console.log('First middleware.!')
//     next();
// });

// app.use((req,res,next) => {
//     console.log('Second middleware.!')
//     res.send('<h1> Check console for middlewares.</h1>');
// });



app.use('/',(req,res,next) => {
    console.log('First middleware.!')
    next();
});

app.use('/bit',(req,res,next) => {
    console.log('bit middleware.!')
    res.send('<h1> Check console for middlewares.</h1>');
});

app.use('/users',(req,res,next) => {
    console.log('users middleware.!')
    res.send('<h1> Check console for middlewares.</h1>');
});



app.listen(3000)