const express = require('express');
const path = require('path');

const router = express.Router();


router.get('/add-items', (req,res,next) => {
    res.sendFile(path.join(__dirname,'../','views','add-items.html'))
});

router.post('/addingItem', (req,res,next) => {
    console.log(req.body);
    res.redirect('/');
});


module.exports = router;