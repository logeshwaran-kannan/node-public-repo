const express = require('express');
const bodyParser = require('body-parser');
const adminRouter = require('./routes/admin4');
const shopRouter = require('./routes/shop4');
const path = require('path');

const app = express();

app.use(bodyParser.urlencoded({extended:false}));

app.use(shopRouter);
app.use('/admin',adminRouter);

app.use((req,res,next) => {
    res.status(404).sendFile(path.join(__dirname,'views','404.html'));
})

app.listen(3000);