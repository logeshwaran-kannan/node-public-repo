const express = require('express');
const routes = express.Router();
const productsController = require('../controllers/productscontroller');

//Shop routing
routes.get('/', productsController.getProducts);

module.exports = routes;
