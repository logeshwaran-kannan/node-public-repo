const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
    const url = req.url;
    const method = req.method;

    if (url === '/form') {
        res.write('<html>');
        res.write('<head><title>Title Info</title></head>');
        res.write('<body><form action="submitform" method="POST"><label>Message:</label><input name="message" type="text"><button type="submit">Submit</button></form></body>');
        res.write('</html>');
        return res.end();
    }

    if (url === '/submitform' && method === "POST") {
        const body = [];
        req.on('data', (data) => {
            console.log(data);
            body.push(data);
        });

        req.on('end', () => {
            const bodyParser = Buffer.concat(body).toString();
            const message = bodyParser.split('=')[1];
            fs.writeFileSync('test.txt', message);
        });
        res.statusCode = 302;
        res.setHeader('Location', '/')
        return res.end();
    }

    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>My Title</title></head>');
    res.write('<body><h1>Welcome Node.js</h1></body>');
    res.write('</html>');
    res.end();

});

server.listen(3000);