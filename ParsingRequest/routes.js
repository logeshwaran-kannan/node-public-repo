const fs = require('fs');


const reqHandler = (req, res) => {
    const url = req.url;
    const method = req.method;
    if (url === '/') {
        res.write('<html>');
        res.write('<head><title>Title 1</title></head>');
        res.write('<body><form action="/formsubmit" method="POST"><label>Message:</label> <input name="message" type="text"><button type="submit">Submit</button></form></body>')
        res.write('</html>')
        return res.end();
    }

    if (url === "/formsubmit" && method === 'POST') {
        const body = [];

        req.on('data', (data) => {
            body.push(data);
        });

        req.on('end', () => {
            const bodyParser = Buffer.concat(body).toString();
            const message = bodyParser.split('=')[1];
            fs.writeFile('text3.txt', message, err => {
                res.statusCode = 302;
                res.setHeader('Location', '/');
                return res.end();
            });
        });
    }
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>Title 1</title></head>');
    res.write('<body><h1>Hello Node.js</h1></body>')
    res.write('</html>')
    res.end();
}


exports.handler = reqHandler;
exports.someText = 'Some Texts';