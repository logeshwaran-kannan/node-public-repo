const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const adminData = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const hanndleBars = require('express-handlebars');

const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(adminData.routes);
app.use(shopRoutes);

app.engine('hbs', hanndleBars());

app.set('view engine','pug');
app.set('views','views');
app.use(express.static(path.join(__dirname,'public')));
app.use((req,res,next) => {
    res.render('404' ,{pageTitle : "Error Page"});
    // res.status(404).sendFile(path.join(__dirname,'views','404.html'));
});

app.listen(3002);