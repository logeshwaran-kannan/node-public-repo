const express = require('express');
const path = require('path');

const routes = express.Router();

const products = [];

// add items -> Get Request
routes.get('/add-items', (req,res,next) => {
    res.render('add-items',{path: '/admin/add-items' , pageTitle:'Add Items'});
    //res.sendFile(path.join(__dirname,'../','views','add-items.html'));
});


// add items -> Post request
routes.post('/add-items', (req,res,next) => {
    res.render(
        'add-items', {path: '/admin/add-items' , pageTitle:'Add Items'});
    // res.sendFile(path.join(__dirname,'../','views','add-items.html'));
});

// Adding Items Routing
routes.post('/addingItems', (req,res,next) => {
    console.log(req.body);
    products.push({ items: req.body.items });
    res.redirect('/');
});

exports.routes = routes;
exports.products = products;