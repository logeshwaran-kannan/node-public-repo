const products = [];

module.exports = class Product {
    constructor(t) {
        this.items = t;
    }

    save(){
        products.push(this);
    }

    static getAllProducts(){
        return products;
    }
}