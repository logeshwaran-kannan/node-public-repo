const fs = require('fs');
const path = require('path');


module.exports = class Product {
    constructor(t) {
        this.items = t;
    }

    save(){
        const datapath = path.join(
            path.dirname(process.mainModule.filename), 
            'data', 
            'products.json'
        );

        fs.readFile(datapath, (err,fileContent) =>{
            let products = [];
            if(!err) {
                products = JSON.parse(fileContent);
            }
            products.push(this);

            fs.writeFile(datapath, JSON.stringify(products), (err) => {
                console.log(err);
            })

        });
    }

    static getAllProducts(){
        const datapath = path.join(
            path.dirname(process.mainModule.filename), 
            'data', 
            'products.json'
        );

        fs.readFile(datapath, (err,fileContent) => {
            if(err){
                return [];
            }
            return JSON.parse(fileContent);
        })
    }
}