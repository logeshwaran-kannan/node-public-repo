const { copyFileSync } = require('fs');
const path = require('path');
const Product = require('../models/productmodel')
exports.getAddItemPage = (req,res,next) => {
    res.render('add-items',{
        path: '/admin/add-items' , 
        pageTitle:'Add Items'
    });
}

exports.postAddItemPage = (req,res,next) => {
    res.render(
        'add-items', {
            path: '/admin/add-items' , 
            pageTitle:'Add Items'
        });
}

exports.addingItems = (req,res,next) => {
    const product = new Product(req.body.items);
    product.save();
    res.redirect('/');
}


