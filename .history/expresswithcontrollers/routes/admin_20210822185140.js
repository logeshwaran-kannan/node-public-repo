const express = require('express');
const path = require('path');
const productsController = require('../controllers/productscontroller');
const routes = express.Router();

const products = [];

// add items -> Get Request
routes.get('/add-items', productsController.getAddItemPage);


// add items -> Post request
routes.post('/add-items', productsController.postAddItemPage);

// Adding Items Routing
routes.post('/addingItems', productsController.addingItems);

exports.routes = routes;
exports.products = products;