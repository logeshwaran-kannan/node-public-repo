const express = require('express');
const productsController = require('../controllers/productscontroller');
const routes = express.Router();

// add items -> Get Request
routes.get('/add-items', productsController.getAddItemPage);

// add items -> Post request
routes.post('/add-items', productsController.postAddItemPage);

// Adding Items Routing
routes.post('/addingItems', productsController.addingItems);

module.exports = routes