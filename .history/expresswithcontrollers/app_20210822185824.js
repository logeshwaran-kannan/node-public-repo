const express = require('express');

const bodyParser = require('body-parser');
const path = require('path');

const adminroutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

const app = express();
app.use(bodyParser.urlencoded({extended:false}));
app.use(adminroutes);
app.use(shopRoutes);

app.set('view engine','ejs');
app.set('views','views');
app.use(express.static(path.join(__dirname,'public')));

app.use((req,res,next) => {
    res.render('404' ,{
        pageTitle : "Error Page", 
        path : ''
    });
});

app.listen(3000);