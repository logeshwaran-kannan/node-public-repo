const products = [];
const path = require('path');

exports.getAddItemPage = (req,res,next) => {
    res.render('add-items',{
        path: '/admin/add-items' , 
        pageTitle:'Add Items'
    });
}

exports.postAddItemPage = (req,res,next) => {
    res.render(
        'add-items', {
            path: '/admin/add-items' , 
            pageTitle:'Add Items'
        });
}

exports.addingItems = (req,res,next) => {
    console.log(req.body);
    products.push({ items: req.body.items });
    res.redirect('/');
}