exports.getAddItemPage = (req,res,next) => {
    res.render('add-items',{
        path: '/admin/add-items' , 
        pageTitle:'Add Items'
    });
}

exports.postAddItemPage = (req,res,next) => {
    res.render(
        'add-items', {
            path: '/admin/add-items' , 
            pageTitle:'Add Items'
        });
}