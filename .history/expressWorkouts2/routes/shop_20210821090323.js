const express = require('express');
const path = require('path');
const adminData = require('./admin');


const routes = express.Router();

routes.get('/', (req,res,next) => {

    res.render('shop',{pageTitle:'Shop',prods : adminData.products, path:'/'});

    //res.sendFile(path.join(__dirname,'../','views','shop.html'));
    // console.log('from shop.js',adminData.products)
});

routes.post('/', (req,res,next) => {
    res.render('shop', {pageTitle:'Shop'});
    //res.sendFile(path.join(__dirname,'../','views','shop.html'));
    console.log('from shop.js',{prods : adminData.products, path:'/'})
});


module.exports = routes;
